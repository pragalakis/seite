export default () => (
  <footer className="montserrat fixed bottom-0 w-100 dn dn-m db-l mt2 bg-white ttu bt b--black-10 no-underline">
    <div className="db ph3">
      <div className="pv2 fl w-50 f6">© 2018</div>
      <div className="pv2 fl w-50 tr f6">mail@mail.com</div>
    </div>
  </footer>
);
