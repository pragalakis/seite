import Head from 'next/head';

export default ({ title }) => (
  <div>
    <Head>
      <link rel="stylesheet" href="../static/css/tachyons.min.css" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <meta charSet="utf-8" />
      <title>{title}</title>
    </Head>
    <style jsx global>{`
      @font-face {
        font-family: 'Montserrat';
        font-weight: normal;
        src: url('../static/fonts/Montserrat-Medium.ttf');
      }

      @font-face {
        font-family: 'Montserrat';
        font-weight: bold;
        src: url('../static/fonts/Montserrat-Bold.ttf');
      }

      .montserrat {
        font-family: 'Montserrat';
        font-weight: normal;
      }

      .montserrat-bold {
        font-family: 'Montserrat';
        font-weight: bold;
      }

      body {
        font-family: 'Montserrat';
        background-color: #ffffff;
        color: #000000;
        margin: 0;
      }
    `}</style>
  </div>
);
