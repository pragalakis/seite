import React from 'react';
import Nav from './nav.js';
import SubNav from './subnav.js';
import MobNav from './mobile-nav.js';
import Logo from './logo.js';

export default class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = { toggle: true };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(state => ({
      toggle: !state.toggle
    }));
    console.log(this.state.toggle);
  }

  render() {
    return (
      <header className="fixed top-0 z-9999 bg-white fl w-100">
        <div className="db h2 mt1 pt1 bb b--black-10">
          <div className="fl w-25 pl3">
            <Logo />
          </div>
          <div className="fl w-75 pr3">
            <div className="dn dn-m db-l montserrat">
              <Nav />
            </div>
            <div className="db db-m dn-l">
              <a
                href="#"
                onClick={this.handleClick}
                className="fixed z-9999 top-0 right-1 mt1 fr center w2 h1 mt1"
              >
                <span className="absolute db w-90 h-25 left-0 top-1 bg-black" />
                <span className="absolute db w-90 mt2 h-25 left-0 top-0 bg-black" />
              </a>
              <div className={this.state.toggle ? 'dn' : 'db'}>
                <MobNav />
              </div>
            </div>
          </div>
        </div>
        <div className="dn dn-m db-l h2 pt1 bb b--black-10 montserrat">
          <SubNav />
        </div>
      </header>
    );
  }
}
