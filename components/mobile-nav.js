import Nav from './nav.js';
import SubNav from './subnav.js';

export default () => (
  <div className="fixed top-0 bottom-0 right-0 bg-white bl b--black-10">
    <div className="pv5 pr3 pl4 montserrat-bold">
      <SubNav />
      <Nav />
      <div className="tr mh2 montserrat mt6 underline-hover pt2 bt bw1 b--black-10">
        mail@mail.com
      </div>
      <div className="tr mh2 ttu montserrat pt3 black-30">© 2018</div>
    </div>
  </div>
);
