import Link from 'next/link';

export default () => (
  <React.Fragment>
    <nav className="tr">
      <ul className="ma0 pa0 dib ttu">
        <li className="v-mid fn fn-m fl-l db mh2 mv2 mv2-m mv0-l" key="about">
          <Link href="/about">
            <a className="lh-copy f5 f5-m f6-l no-underline black dim">about</a>
          </Link>
        </li>
        <li
          className="v-mid fn fn-m fl-l fl db mh2 pl4 mv2 mv2-m mv0-l"
          key="custom"
        >
          <Link href="/custom">
            <a className="lh-copy f5 f5-m f6-l no-underline black dim">
              Custom Work
            </a>
          </Link>
        </li>
      </ul>
    </nav>
  </React.Fragment>
);
