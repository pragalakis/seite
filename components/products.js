import Link from 'next/link';
import data from '../data/products.json';

export default ({ products }) => (
  <div className="cf">
    {products.map(product => (
      <div className="fl w-100 w-50-m w-25-l pa2 mv3 grow" key={product}>
        <div className="pa1 bg-white ba black-10">
          <figure className="ma3">
            <Link href={`/product/?name=${product}`} as={`/product/${product}`}>
              <a className="db link tc">
                <div
                  className="bg-center cover w-100 h5 overflow-hidden"
                  style={{
                    backgroundImage: `url(../static/images/${
                      data.products[product].images[0]
                    }.jpg)`
                  }}
                />
              </a>
            </Link>
          </figure>
          <div className="mh3">
            <Link href={`/product/?name=${product}`} as={`/product/${product}`}>
              <a className="no-underline black ttc montserrat">
                {data.products[product].title}
              </a>
            </Link>
            <p className="mv2 black-30">
              {data.products[product]['short-description']}
            </p>
          </div>
        </div>
      </div>
    ))}
  </div>
);
