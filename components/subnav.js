import Link from 'next/link';
import data from '../data/products.json';

const categories = data.categories;

export default () => (
  <nav className="tr tr-m tl-l">
    <ul className="ma0 pa0 dib ttu ttu-m ttn-l">
      {Object.keys(categories).map(cat => (
        <li
          className="v-mid fn fn-m fl-l db mh2 mh2-m mv2 mv2-m mv0-l ml3-l pr0 pr0-m pr4-l"
          key={cat}
        >
          <Link href={`/${cat}`}>
            <a className="lh-copy f5 f5-m f6-l no-underline black dim">{cat}</a>
          </Link>
        </li>
      ))}
    </ul>
  </nav>
);
