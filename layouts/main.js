import Header from '../components/header.js';
import Footer from '../components/footer.js';

export default ({ children }) => (
  <React.Fragment>
    <Header />
    <div className="vh-100 ph3 pt5 sans-serif">{children}</div>
    <Footer />
  </React.Fragment>
);
