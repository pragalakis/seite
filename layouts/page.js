import Head from '../components/head.js';
import Header from '../components/header.js';
import Footer from '../components/footer.js';

export default ({ children, title }) => (
  <React.Fragment>
    <Head title={title} />
    <Header />

    <div className="sans-serif overflow-auto ph3 pt3 pt5-l pb5 mt2">
      <h1 className="f1 montserrat-bold ttc">{title}</h1>
      {children}
    </div>

    <Footer />
  </React.Fragment>
);
