// next.config.js
// Dynamic generated pages
// check the products.json data on ./data
module.exports = {
  exportPathMap: async function(defaultPathMap) {
    return {
      '/': { page: '/' },
      '/about': { page: '/about' },
      '/custom': { page: '/custom' },
      '/one': { page: '/one', query: { name: 'one' } },
      '/two': { page: '/two', query: { name: 'two' } },
      '/three': { page: '/three', query: { name: 'three' } },
      '/four': { page: '/four', query: { name: 'four' } },
      '/product/product1': { page: '/product', query: { name: 'product1' } },
      '/product/product2': { page: '/product', query: { name: 'product2' } },
      '/product/product3': { page: '/product', query: { name: 'product3' } },
      '/product/product4': { page: '/product', query: { name: 'product4' } },
      '/product/product6': { page: '/product', query: { name: 'product6' } },
      '/product/product7': { page: '/product', query: { name: 'product7' } },
      '/product/product8': { page: '/product', query: { name: 'product8' } }
    };
  }
};
