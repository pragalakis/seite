import React from 'react';
import Head from '../components/head.js';
import Main from '../layouts/main.js';

export default () => (
  <React.Fragment>
    <Head title="homepage" />
    <Main>
      <article className="montserrat">
        <div className="pt3 pt4-m pt6-l dt w-100 tc black cover">
          <div className="dtc v-mid">
            <h3 className="f6 fw1 lh-title">Upper title</h3>
            <h1 className="f1 f-headline-l fw1 i black-60">
              Highlighted Category
            </h1>
            <blockquote className="ph0 mh0 measure f4 lh-copy center">
              <p className="fw1 black-70">
                Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,
                consectetur, adipisci velit...
              </p>
              <p className="f6 ttu tracked fs-normal">Name Lastname</p>
            </blockquote>
          </div>
        </div>
      </article>
    </Main>
  </React.Fragment>
);
