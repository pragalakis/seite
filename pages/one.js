import React from 'react';
import Products from '../components/products.js';
import data from '../data/products.json';
import Page from '../layouts/page.js';

export default class extends React.Component {
  static async getInitialProps({ pathname }) {
    return { pathname };
  }
  render() {
    const name = this.props.pathname.substring(1);
    const title = data.categories[name].title;
    const description = data.categories[name].description;
    const products = data.categories[name].products;
    return (
      <React.Fragment>
        <Page title={title}>
          <p className="black-30 mt0">{description}</p>
          <Products products={products} />
        </Page>
      </React.Fragment>
    );
  }
}
