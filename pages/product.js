import React from 'react';
import Page from '../layouts/page.js';
import data from '../data/products.json';

export default class extends React.Component {
  static async getInitialProps({ query }) {
    return { query };
  }
  render() {
    const name = this.props.query.name;
    const product = data.products[name];
    const title = product.title;
    const description = product.description;
    return (
      <React.Fragment>
        <Page title={title}>
          <div className="relative pt1 pt4-l">
            <div className="fl w-100 w-25-l">
              <div className="mr2 mb2 mb2-m mb2-ns pa2 ba b--black-10">
                <p className="bb b--black-10 montserrat t1 w-60">
                  <span className="f4">Description</span>
                </p>
                <p>{description}</p>

                <p className="bb b--black-10 montserrat pt4 w-60">
                  <span className="f4">Info</span>
                </p>
                <p>
                  <span className="b">Materials:</span>{' '}
                  {product.attributes.materials}
                </p>
                <p>
                  <span className="b">Measurements:</span>{' '}
                  {product.attributes.measurements}
                </p>
                <p>
                  <span className="b">
                    {product.attributes.available
                      ? `Price: ${product.attributes.price}`
                      : 'sold out'}
                  </span>
                </p>
              </div>
            </div>
            <div className="relative fl w-100 w-75-l">
              {product.images.slice(1).map((img, i) => (
                <div
                  className="fl w-50 w-33-l pl2 pr2-m ph2-l mb3 mb4-l"
                  key={i}
                >
                  <div
                    className="cover pv5 pv6-m pv7-l"
                    style={{
                      background: `black url(../static/images/${img}.jpg) center`
                    }}
                  />
                </div>
              ))}
            </div>
          </div>
        </Page>
      </React.Fragment>
    );
  }
}
